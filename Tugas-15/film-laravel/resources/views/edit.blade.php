<!DOCTYPE html>
<html>
<head>
	<title>CRUD Film di Laravel</title>
</head>
<body>

	<h3>Edit Pemain film</h3>
	<br/>
	<br/>

	@foreach($cast as $c)
	<form action="/cast/update" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $c->id }}"> <br/>
        <label for='firstname'>Nama :</label><br><br>
		<input type="text" name="name" value="{{ $c->name }}"> <br><br>
		<label for='firstname'>Umur :</label><br><br>
        <input type="number" name="umur" value="{{ $c->umur }}"> <br><br>
        <label for='firstname'>Bio :</label><br><br>
		<textarea name="bio" cols="30" rows="10">{{ $c->bio }}</textarea> <br><br>
		<input type="submit" value="Update Data">
	</form>
	@endforeach
    <a href="/cast"> Kembali</a>
</body>
</html>
