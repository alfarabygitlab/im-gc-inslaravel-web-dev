<?php

use App\Http\Controllers\CastController; //wajib ditambahkan jika menggunakan laravel versi 8 atau 9(terbaru) utk diarahkan ke Controllers yang dibuat
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast/{id}', [CastController::class, 'show']);
Route::get('/cast/edit/{id}', [CastController::class, 'edit']);
Route::post('/cast/update', [CastController::class, 'update']);
Route::get('/cast/destroy/{id}', [CastController::class, 'destroy']);
