<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CastController extends Controller
{
    //membuat function crud

    public function index()
    {
        // mengambil data dari table cast
        $cast = DB::table('cast')->get();

        // mengirim data cast ke view index
        return view('index', ['cast' => $cast]);
    }

    public function create()
    {

        // memanggil view create
        return view('create');
    }

    // method untuk insert data ke table cast
    public function store(Request $request)
    {
        // insert data ke table cast
        DB::table('cast')->insert([
            'name' => $request->name,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);
        // alihkan ke halaman index
        return redirect('/cast');
    }

    // method untuk edit data pemain film
    public function edit($id)
    {
        // mengambil data pemain film berdasarkan id yang dipilih
        $cast = DB::table('cast')->where('id', $id)->get();
        // passing data pemain film yang didapat ke view edit.blade.php
        return view('edit', ['cast' => $cast]);
    }

    // update data pegawai
    public function update(Request $request)
    {
        // update data pegawai
        DB::table('cast')->where('id', $request->id)->update([
            'name' => $request->name,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ]);
        // alihkan halaman ke halaman index
        return redirect('/cast');
    }

    // method untuk hapus data pemain film
    public function destroy($id)
    {
        // menghapus data pemain film berdasarkan id yang dipilih
        DB::table('cast')->where('id', $id)->delete();

        // alihkan halaman ke halaman index
        return redirect('/cast');
    }
}
